.. _installation:

Installation
============

Bohrium supports linux, Mac OSX and Windows.

.. toctree::
   :maxdepth: 1
 
   linux
   osx
   windows

